package com.guo.www.ui;

import java.awt.Color;
import java.awt.Frame;
import java.awt.Label;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class MainUI {
	
	private Frame mainFrame;  //主界面
	
	/**
	 * 设置界面内容
	 */
	private void initUI() {
		Frame f = new Frame("根证书申请");
		f.setBounds(30, 30, 400, 600);
		f.setVisible(true);
		Label text = new Label("欢迎使用CA签发工具");
		f.add(text);
		
		
		f.addWindowListener(new WindowAdapter() {
			
			@Override
			public void windowClosing(WindowEvent e) {
				System.exit(0);
				super.windowClosed(e);
			}
			
		});
	}
	
	
	
	
	
	public static void main(String[] args) {
		MainUI ui = new MainUI();
		ui.initUI();
	}
}
