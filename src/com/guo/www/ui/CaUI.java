package com.guo.www.ui;


import java.awt.*;
import javax.swing.*;


public class CaUI{

	static final int WIDTH = 400;
	static final int HEIGHT = 600;
	
	JFrame caFrame;
	JButton btnSure;    //确定按钮
	JButton btnCancel;  //取消按钮
	
	public CaUI() {
		caFrame = new JFrame("CA根证书申请");
		caFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		GridLayout lay = new GridLayout(1,2);
		caFrame.setLayout(lay);
		caFrame.setLocation(200, 200);
		caFrame.setSize(new Dimension(WIDTH, HEIGHT));
		
		btnSure = new JButton("确定");
		btnCancel = new JButton("取消");
				
		/**
		 * Country Name (2 letter code) [XX]:      # 两个字符表示的国家代码，CN为中国
         * State or Province Name (full name) []:      # 省或洲的完整名称
         * Locality Name (eg, city) [Default City]:      # 所在位置的名称(默认为城市)
         * Organization Name (eg, company) [Default Company Ltd]:     # 组织机构名称(默认为公司)
         * Organizational Unit Name (eg, section) []:     # 组织机构单元名称(eg.部门)
         * Common Name (eg, your name or your server's hostname) []:     # 持有者名或者所在服务器主机名(即域名)
         * Email Address []:     # 管理员邮件地址，可以省略
		 */
		JPanel p1 = new JPanel();
		p1.setLayout(new GridLayout(7, 1));
		JLabel title = new JLabel("CA根证书申请");
		JLabel countryName = new JLabel("国家");
		JLabel provinceName = new JLabel("省份");
		JLabel localityName = new JLabel("城市");
		JLabel organizationName = new JLabel("组织");
		JLabel organizationalUnitName  = new JLabel("部门");
		JLabel commonName  = new JLabel("姓名");
	
		p1.add(countryName);
		p1.add(provinceName);
		p1.add(localityName);
		p1.add(organizationName);
		p1.add(organizationalUnitName);
		p1.add(commonName);
		p1.add(btnSure);
		
		JPanel p2 = new JPanel();
		p2.setLayout(new GridLayout(7, 1));
		final JTextField countryNameText = new JTextField();
		final JTextField provinceNameText = new JTextField();
		final JTextField localityNameText = new JTextField();
		final JTextField organizationNameText = new JTextField();
		final JTextField organizationalUnitNameText = new JTextField();
		final JTextField commonNameText = new JTextField();
		p2.add(countryNameText);
		p2.add(provinceNameText);
		p2.add(localityNameText);
		p2.add(organizationNameText);
		p2.add(organizationalUnitNameText);
		p2.add(commonNameText);
		p2.add(btnCancel);
		caFrame.add(p1);
		caFrame.add(p2);
		
		caFrame.setVisible(true);
	    		
	}
	public static void main(String[] args) {
		new CaUI();
	}
}
