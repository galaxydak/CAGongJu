package com.guo.www.util;

import java.math.BigInteger;
import java.util.Date;
import java.util.Random;



public class CertUtil {

	public static BigInteger genCertSerial() {
		byte[] b = new byte[32];
		Random random = new Random(new Date().getTime());
		for(int i=0;i<32;i++) {
			byte[] temp = new byte[10];
			random.nextBytes(temp);
			b[i] = temp[random.nextInt(temp.length-1)];
		}
		return new BigInteger(b);
	}
	
}
