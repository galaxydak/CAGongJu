package com.guo.www.util;

import java.util.Calendar;
import java.util.Date;

/**
 * 
 * @author Administrator
 * 时间工具
 */
public class DateUtil {
	public static Calendar c = Calendar.getInstance();
	/**
	 * 获取当前时间
	 * @return
	 */
	public static Date getCurrDate() {
		Date date = null;
		date = c.getTime();
		return date;
	}
	
	/**
	 * 获取
	 * @param year
	 * @return
	 */
	public static Date getXYear(int year) {
		Date date = null;
		c.add(Calendar.YEAR, year);
		date = c.getTime();
		return date;
	}
	
	
}
