package com.guo.www.pkcs12;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.Security;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class PkcsTools {
	
	static {
		Security.addProvider(new BouncyCastleProvider());
	}
	
	/*
	 * 获取KeyStore
	 * String path keystore路径
	 * String passWord keystore密码 
	 */
	public static KeyStore getStore(String path, String passWord) {
		try {
			KeyStore keyStore = KeyStore.getInstance("pkcs12");
			File storeFile = new File(path);
			InputStream stream = new FileInputStream(storeFile);
			keyStore.load(stream, passWord.toCharArray());
			return keyStore;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * 获取KeyStore
	 * 默认获取KeyStore
	 */
	public static KeyStore getStore() {
		try {
			KeyStore keyStore = KeyStore.getInstance("pkcs12");
			keyStore.load(null, null);
			return keyStore;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/*
	 * 获取keypair
	 * int keySize 公私钥对长度
	 */
	public static KeyPair getKeyPair(int keySize) {
		KeyPairGenerator kpg;
		try {
			KeyPairGenerator  kpGen = KeyPairGenerator.getInstance("RSA", "BC");
		    kpGen.initialize(1024, new SecureRandom());
			KeyPair keyPair = kpGen.generateKeyPair();
			return keyPair;
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchProviderException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
