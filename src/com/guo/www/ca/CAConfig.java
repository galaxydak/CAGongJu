package com.guo.www.ca;

public interface CAConfig {
	String CA_C="CN";
	String CA_ST="SC";
	String CA_L="CD";
	String CA_O="WST";
	String CA_ROOT_ISSUER="C=CN,ST=SC,L=CD,O=WST,OU=WSTCA,CN=WST";
	String CA_DEFAULT_SUBJECT="C=CN,ST=SC,L=CD,O=WST,OU=WSTCA,CN=";
	String CA_SHA="SHA256WithRSAEncryption";
}
