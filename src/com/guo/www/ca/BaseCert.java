package com.guo.www.ca;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.security.cert.X509Certificate;

import javax.security.auth.x500.X500Principal;

import org.bouncycastle.asn1.x509.V3TBSCertificateGenerator;
import org.bouncycastle.jce.X509Principal;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.x509.X509V3CertificateGenerator;

import com.guo.www.util.CertUtil;
import com.guo.www.util.DateUtil;

public class BaseCert {

	static {
		Security.addProvider(new BouncyCastleProvider());
	}

	protected static KeyPairGenerator kpg = null;

	public BaseCert() {
		try {
			kpg = KeyPairGenerator.getInstance("RSA");
			kpg.initialize(1024);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("deprecation")
	public X509Certificate generateCert(String user) {
		X509Certificate cert = null;
		try {
			KeyPair keyPair = this.kpg.generateKeyPair();
			// 获取公钥
			PublicKey pubKey = keyPair.getPublic();
			// 获取私钥
			PrivateKey priKey = keyPair.getPrivate();

			X509V3CertificateGenerator certGen = new X509V3CertificateGenerator();
			// 设置序列号
			certGen.setSerialNumber(CertUtil.genCertSerial());
			// 设置颁发者
			certGen.setIssuerDN(new X509Principal(CAConfig.CA_ROOT_ISSUER));
			// 设置有效期
			certGen.setNotBefore(DateUtil.getCurrDate());
			certGen.setNotAfter(DateUtil.getXYear(3));
			// 设置使用者
			certGen.setSubjectDN(new X500Principal(CAConfig.CA_DEFAULT_SUBJECT + user));
			// 设置公钥
			certGen.setPublicKey(pubKey);
			// 签名算法
			certGen.setSignatureAlgorithm(CAConfig.CA_SHA);
			cert = certGen.generateX509Certificate(priKey, "BC");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cert;
	}
}
