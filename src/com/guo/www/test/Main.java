package com.guo.www.test;

import java.beans.Expression;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.Scanner;

import com.guo.www.pkcs12.Pkcs12;

public class Main {
	
	
	
	
	//private static String certPath = "d:/ca.cer";
	public static void main(String[] args) {
		System.out.println("*********************************");
		System.out.println("*************自制CA系统************");
		System.out.println("*********************************");
		System.out.println("1.根证书申请");
		System.out.println("2.证书签发");
		System.out.println("3.退出系统");
		System.out.println("请输入选择：");
		try {
			Scanner sc = new Scanner(System.in);
			String choice = sc.nextLine();
			int choiceNum = Integer.valueOf(choice);
			if(choiceNum==1) {
				System.out.println("******根证书申请********");
				System.out.println("请输入根证书路径：(默认当前路径)");
				String  path = sc.nextLine();
				if(path.isEmpty()) {
					File file = new File("");	
					path = file.getAbsolutePath();
					System.out.println("请输入根证书路径："+path);
				}
				
				System.out.println("请输入文件名：");
				String fileName = sc.nextLine();
				
				System.out.println("请输入CA密码：");
				String password = sc.nextLine();
				
				System.out.println("请输入国家代码：");
				String countryName = sc.nextLine();
				
				System.out.println("请输入省份名称：");
				String provinceName  = sc.nextLine();
				
				System.out.println("请输入城市：");
				String localityName = sc.nextLine();
				
				System.out.println("请输入组织机构名称：");
				String organizationName = sc.nextLine();
				
				System.out.println("请输入部门名称：");
				String organizationalUnitName = sc.nextLine();
				
				System.out.println("请输入持有者名或者所在服务器主机名：");
				String commonName = sc.nextLine();
				
				String issuer = "C="+countryName+",ST="+provinceName+",L="+localityName+
						",O="+organizationName+",OU="+organizationalUnitName+",CN="+commonName;
				Pkcs12 pkcs12 = new Pkcs12();
				pkcs12.genRootCA(path, fileName, password, issuer);
				System.out.println("签发成功！");
			}else if(choiceNum==2) {
				System.out.println("*******证书签发********");
			}else if(choiceNum==3) {
				System.out.println("*******退出系统********");
			}
			
			//BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		
	}
}
